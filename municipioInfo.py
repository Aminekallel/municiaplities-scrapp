import requests
from bs4 import BeautifulSoup
import re
import json
state_codes={'Aguascalientes': '01', 'Baja California': '02', 'Baja California Sur': '03', 'Campeche': '04', 'Coahuila': '05', 'Colima': '06', 'Chiapas': '07', 'Chihuahua': '08', 'Ciudad de México': '09', 'Durango': '10', 'Guanajuato': '11', 'Guerrero': '12', 'Hidalgo': '13', 'Jalisco': '14', 'Estado de México': '15', 'Michoacán': '16', 'Morelos': '17', 'Nayarit': '18', 'Nuevo León': '19', 'Oaxaca': '20', 'Puebla': '21', 'Querétaro': '22', 'Quintana Roo': '23', 'San Luis Potosí': '24', 'Sinaloa': '25', 'Sonora': '26', 'Tabasco': '27', 'Tamaulipas': '28', 'Tlaxcala': '29', 'Veracruz': '30', 'Yucatán': '31', 'Zacatecas': '32'}
def getInfo(municipality):
    if municipality["link"]=="NULL":
        municipality=brokenLink(municipality)
    else:
        i=0
        while(i<5):
            try:
                x=requests.get("https://es.wikipedia.org"+municipality["link"])
            except:
                i+=1
            else:
                break
        soup = BeautifulSoup(x.text, 'html.parser')
        table=soup.find_all("table",class_="infobox")
        if(len(table)==0):
            municipality=brokenLink(municipality)
        else:
            table=table[0]
            if(municipality["id_code"]=="-1"):
                municipality["id_code"]=getIdCode(table)
            municipality["superficie"]=getSuperficie(table)
            municipality["cp"]=getCp(table)
            municipality["indicatif_telephonique"]=getIndicatifTelephonique(table)
            municipality["gentile"]=getGentile(table)
            municipality["altitud"]=getAltitud(table)
            municipality["site"]=getSite(table)
            municipality["maire"]=getMaire(table)
            municipality["politique"]=getPolitique(table)
        municipality["wikipedia_id"]=getWikipediaId(soup)
        municipality["wikipedia_en_id"]=getWikipediaEnId(soup)
        municipality["wikidata"]=getWikidataId(soup)
    municipality=getFromInegi(municipality)
    return municipality
def getFromInegi(municipality):
    i=0
    while(i<5):
        try:
            req=requests.get("https://gaia.inegi.org.mx/wscatgeo/mgem/"+state_codes[municipality["region_name"]]+"/"+municipality["id_code"])
        except:
            i+=1
        else:
            break
    data=json.loads(req.text)
    if("datos" in data):
        data=data["datos"][0]
        if("pob" in data):
            municipality["habitants"]=municipality.get("habitants",data["pob"])
        if("pob_mas" in data):
            municipality["habitants_m"]=municipality.get("habitants_m",data["pob_mas"])
        if("pob_fem" in data):
            municipality["habitants_f"]=municipality.get("habitants_f",data["pob_fem"])
    return municipality

def getHabitants(link):
    i=0
    while(i<5):
        try:
            req=requests.get("https://es.wikipedia.org"+link+"?action=edit")
        except:
            i+=1
        else:
            break
    soup1 = BeautifulSoup(req.text, 'html.parser')
    textarea=soup1.find("textarea").text.split("\n}}")[0].split("|")
    habitants="NULL"
    habitants_m="NULL"
    habitants_f="NULL"
    for line in textarea:
        if("población_h" in line):
            habitants_m=line.split("=")[1].strip()
        if("población_m" in line):
            habitants_f=line.split("=")[1].strip()
        match=re.findall("población(\s)*=(\s)*",line)
        if(match!=[]):
            habitants=line.split("=")[1].strip()
    return habitants,habitants_m,habitants_f
def getPolitique(table):
    presidente=table.find(string=["Presidente Municipal","Presidente municipal","Pdte. municipal","Presidenta Municipal","Presidenta municipal","Pdta. municipal"])
    if(presidente):
        politique_td=presidente.find_parent("tr").td.img
        if(politique_td):
            politique=politique_td["alt"].replace("logo","")
            politique=re.sub("\((.*?)\)\.svg","",politique).strip()
            return politique
    return "NULL"
def getMaire(table):
    presidente=table.find(string=["Presidente Municipal","Presidente municipal","Pdte. municipal","Presidenta Municipal","Presidenta municipal","Pdta. municipal"])
    if(presidente):
        presidente_td=presidente.find_parent("tr").td
        presidente_text=presidente_td.text
        presidente_text=re.sub("\[(.*?)\]","",presidente_text).replace("\u200b","")
        presidente_text=re.sub("\((.*?)\)","",presidente_text)
        presidente_text=re.sub("https\://(\w|\.|/)+","",presidente_text)
        presidente_text=re.sub("(\d)+\-(\d+)","",presidente_text).strip()
        return presidente_text
    return "NULL"
def getWikidataId(soup):
    script=soup.find("script").string
    RLCONF=json.loads(script.split("RLCONF=")[1].split(";")[0].replace("\n",""))
    if "wgWikibaseItemId" in RLCONF:
        return RLCONF["wgWikibaseItemId"]
    return "NULL"
def getWikipediaEnId(soup):
    link=soup.find("a",{"lang":"en"})
    if(link):
        req=requests.get(link["href"])
        soup1=BeautifulSoup(req.text, 'html.parser')
        wikiId=getWikipediaId(soup1)
        return wikiId
    return "NULL"
def getWikipediaId(soup):
    script=soup.find("script").string
    wikiId=re.findall('\"wgRelevantArticleId\"\:(.*?)\,',script)
    return wikiId
def getSite(table):
    site_link=table.find("a",string="Sitio web oficial ")
    if(site_link):
        return site_link["href"]
    return "NULL"
def getAltitud(table):
    altitud_link=table.find("a",string="Altitud")
    if(altitud_link):
        altitud_tr=altitud_link.find_parent("tr")
        if ("Media" in altitud_tr.next_sibling.th.text):
            return altitud_tr.next_sibling.td.contents[0].strip()
    return "NULL"
def getGentile(table):
    gentile_link=table.find("a",string="Gentilicio")
    if(gentile_link):
        gentile_tr=gentile_link.find_parent("tr")
        return gentile_tr.td.contents[0].strip().replace("\xa0"," ")
    else:
        return "NULL"
def getIndicatifTelephonique(table):
    indicatif_link=table.find("a",string="Clave Lada")
    if(indicatif_link):
        indicatif_tr=indicatif_link.find_parent("tr")
        return indicatif_tr.td.contents[0].strip().replace("\xa0"," ")
    else:
        return "NULL"
def getIdCode(table):
    inegi_link=table.find("a",string="Código INEGI")
    inegi_tr=inegi_link.find_parent("tr")
    return inegi_tr.td.contents[0].strip()[2:5]
def getCp(table):
    cp_link=table.find("a",string="Código postal")
    cp_text=table.find("span",class_="postal-code")
    if(cp_link!=cp_text and cp_text==None):
            return cp_link.find_parent("tr").td.text.strip()
    elif(cp_text!=None):
        if(cp_text.contents[0].strip()==''):
            return cp_link.find_parent("tr").td.li.contents[0]
        else:
            return cp_text.contents[0].strip()
    else:
        return "NULL"
def getSuperficie(table):
    superficie_link=table.find("a",string="Superficie")
    if(superficie_link!=None):
        superficie_tr=superficie_link.find_parent("tr")
        return superficie_tr.next_sibling.td.contents[0].strip().replace(" km","").replace("²","").replace(",",".")
    else:
        return "NULL"
def brokenLink(municipality):
    municipality["wikipedia"]="Municipio de "+municipality["wikipedia"]
    municipality["cvegeo"]=state_codes[municipality["region_name"]]+municipality["id_code"]
    municipality["cp"]=municipality.get("cp","NULL")
    municipality["indicatif_telephonique"]=municipality.get("indicatif_telephonique","NULL")
    municipality["gentile"]=municipality.get("gentile","NULL")
    municipality["superficie"]=municipality.get("superficie","NULL")
    municipality["altitude"]=municipality.get("altitude","NULL")
    municipality["maire"]=municipality.get("maire","NULL")
    municipality["politique"]=municipality.get("politique","NULL")
    municipality["site"]=municipality.get("site","NULL")
    municipality["wikipedia_id"]=municipality.get("wikipedia_id","NULL")
    municipality["wikipedia_en_id"]=municipality.get("wikipedia_en_id","NULL")
    municipality["wikidata"]=municipality.get("wikidata","NULL")
    municipality["habitants"]=municipality.get("habitants","NULL")
    municipality["habitants_m"]=municipality.get("habitants_m","NULL")
    municipality["habitants_f"]=municipality.get("habitants_f","NULL")
    return municipality
