import requests
from bs4 import BeautifulSoup
def format1(region_name,url):
    x=requests.get(url)
    soup = BeautifulSoup(x.text, 'html.parser')
    table=soup.find("table",class_="toc")
    rows=table.find_all("tr")[3:]
    municipalities=[]
    for row in rows:
        columns=row.find_all("td")
        id_code=columns[0].text.strip()
        wikipedia=columns[1].text.strip()
        Type="Municipio"
        if(columns[1].a.has_attr("class") and "new" in columns[1].a["class"]):
            if(columns[2].a.has_attr("class") and "new" in columns[2].a["class"]):
                link="NULL"
            else:
                link=columns[2].a["href"]
                Type="Cabecera Municipal"
        else:
            link=columns[1].a["href"]
        municipalities.append({"region_name":region_name,"id_code":id_code,"wikipedia":wikipedia,"link":link,"type":Type})
        if(len(columns)>=6 and columns[4].a != None):
            id_code=columns[3].text.strip()
            wikipedia=columns[4].text.strip()
            if(columns[4].a.has_attr("class") and "new" in columns[4].a["class"]):
                if(columns[5].a.has_attr("class") and "new" in columns[5].a["class"]):
                    link="NULL"
                else:
                    link=columns[5].a["href"]
                    Type="Cabecera Municipal"
            else:
                link=columns[4].a["href"]
            municipalities.append({"region_name":region_name,"id_code":id_code,"wikipedia":wikipedia,"link":link,"type":Type})
    return municipalities
def format2(region_name,url):
    municipio_column=2
    cabecara_column=3
    if(region_name=="Guerrero"):
        municipio_column=3
        cabecara_column=2
    x=requests.get(url)
    soup = BeautifulSoup(x.text, 'html.parser')
    table=soup.select("table.wikitable.sortable")[0]
    if(region_name=="Puebla"):
        rows=table.find_all("tr")[1:-1]
    else:
        rows=table.find_all("tr")[1:]
    municipalities=[]
    for row in rows:
        columns=row.find_all("td")
        id_code=columns[0].text.strip()
        Type="Municipio"
        if(region_name=="Chiapas" and id_code=="095"):
            municipalities.append({"region_name":region_name,"id_code":id_code,"wikipedia":"Belisario Domínguez","link":"NULL","type":Type})
            continue
        wikipedia=columns[municipio_column].text.strip()
        if(columns[municipio_column].a == None or (columns[municipio_column].a.has_attr("class") and "new" in columns[municipio_column].a["class"])):
            if(columns[cabecara_column].a == None or (columns[cabecara_column].a.has_attr("class") and "new" in columns[cabecara_column].a["class"])):
                link="NULL"
            else:
                link=columns[cabecara_column].a["href"]
                Type="Cabecera Municipal"
        else:
            link=columns[municipio_column].a["href"]
        municipalities.append({"region_name":region_name,"id_code":id_code,"wikipedia":wikipedia,"link":link,"type":Type})
    return municipalities
def format3(region_name,url):
    municipio_column=1
    cabecara_column=2
    x=requests.get(url)
    soup = BeautifulSoup(x.text, 'html.parser')
    table=soup.select("table.wikitable.sortable")[0]
    rows=table.find_all("tr")[1:]
    municipalities=[]
    for row in rows:
        columns=row.find_all("td")
        id_code=columns[0].text.strip()
        Type="Municipio"
        wikipedia=columns[municipio_column].text.strip()
        if(columns[municipio_column].a.has_attr("class") and ("new" in columns[municipio_column].a["class"] or "external" in columns[municipio_column].a["class"])):
            if(columns[cabecara_column].a.has_attr("class") and "new" in columns[cabecara_column].a["class"]):
                link="NULL"
            else:
                link=columns[cabecara_column].a["href"]
                Type="Cabecera Municipal"
        else:
            link=columns[municipio_column].a["href"]
        if(region_name=="Nuevo León" and link=="NULL"):
            habitants=columns[5].span.next_sibling.strip().replace("\xa0","")
            superficie=str(float(columns[6].text.strip().replace(",","."))/100)
            municipalities.append({"region_name":region_name,"id_code":id_code,"wikipedia":wikipedia,"link":link,"habitants":habitants,"superficie":superficie,"type":Type})
        else:
            municipalities.append({"region_name":region_name,"id_code":id_code,"wikipedia":wikipedia,"link":link,"type":Type})
    return municipalities
def format4(region_name,url):
    municipio_column=1
    cabecara_column=2
    x=requests.get(url)
    soup = BeautifulSoup(x.text, 'html.parser')
    if(region_name=="Ciudad de México"):
        table=soup.find("table",{"class":["wikitable"],"align":"center"})
    else:
        table=soup.find("table",{"class":["wikitable"]})
    rows=table.find_all("tr")[1:]
    municipalities=[]
    for row in rows:
        columns=row.find_all("td")
        id_code=columns[0].text.strip()
        if(region_name=="Ciudad de México"):
            id_code="-1"
        wikipedia=columns[municipio_column].text.strip()
        Type="Municipio"
        if(columns[municipio_column].a.has_attr("class") and "new" in columns[municipio_column].a["class"]):
            if(columns[cabecara_column].a.has_attr("class") and "new" in columns[cabecara_column].a["class"]):
                link="NULL"
            else:
                link=columns[cabecara_column].a["href"]
                Type="Cabecera Municipal"
        else:
            link=columns[municipio_column].a["href"]
        municipalities.append({"region_name":region_name,"id_code":id_code,"wikipedia":wikipedia,"link":link,"type":Type})
    return municipalities