import municipioLinks
import municipioInfo
import pickle
import time
import sys
sys.setrecursionlimit(50000)
start_time = time.time()
states={'Aguascalientes': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Aguascalientes', 'Baja California': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Baja_California', 'Baja California Sur': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Baja_California_Sur', 'Campeche': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Campeche', 'Chiapas': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Chiapas', 'Chihuahua': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Chihuahua', 'Ciudad de México': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Ciudad_de_M%C3%A9xico', 'Coahuila': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Coahuila', 'Colima': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Colima', 'Durango': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Durango', 'Guanajuato': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Guanajuato', 'Guerrero': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Guerrero', 'Hidalgo': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Hidalgo', 'Jalisco': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Jalisco', 'Estado de México': 'https://es.wikipedia.org/wiki/Anexo:Municipios_del_estado_de_M%C3%A9xico', 'Michoacán': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Michoac%C3%A1n', 'Morelos': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Morelos', 'Nayarit': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Nayarit', 'Nuevo León': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Nuevo_Le%C3%B3n', 'Oaxaca': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Oaxaca', 'Puebla': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Puebla', 'Querétaro': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Quer%C3%A9taro', 'Quintana Roo': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Quintana_Roo', 'San Luis Potosí': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_San_Luis_Potos%C3%AD', 'Sinaloa': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Sinaloa', 'Sonora': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Sonora', 'Tabasco': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Tabasco', 'Tamaulipas': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Tamaulipas', 'Tlaxcala': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Tlaxcala', 'Veracruz': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Veracruz', 'Yucatán': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Yucat%C3%A1n', 'Zacatecas': 'https://es.wikipedia.org/wiki/Anexo:Municipios_de_Zacatecas'}
format1=["Aguascalientes","Colima","Nayarit","Oaxaca","Tlaxcala","Yucatán","Sinaloa"]
format2=["Baja California","Chiapas","Chihuahua","Durango","Jalisco","Michoacán","Querétaro","Sonora","Zacatecas","Guerrero","Campeche","Puebla","Quintana Roo","Veracruz"]
format3=["Hidalgo","Coahuila","Morelos","Nuevo León","Baja California Sur","San Luis Potosí","Tamaulipas","Tabasco","Estado de México"]
format4=["Ciudad de México","Guanajuato"]
municipalities=[]
for format in states:
    if format in format1:
        municipalities+=municipioLinks.format1(format,states[format])
    if format in format2:
        municipalities+=municipioLinks.format2(format,states[format])
    if format in format3:
        municipalities+=municipioLinks.format3(format,states[format])
    if format in format4:
        municipalities+=municipioLinks.format4(format,states[format])
final_municipalities=[]
i=1
total=len(municipalities)
for municipality in municipalities:
    final_municipalities.append(municipioInfo.getInfo(municipality))
    print(str(i)+"/"+str(total))
    i+=1
f=open("final.txt","wb")
serialized = pickle.dump(final_municipalities,f)
f.close()
print("--- %s seconds ---" % (time.time() - start_time))
